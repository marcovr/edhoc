#include "../testhelper.h"
#include <unity.h>
#include <edhoc.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <oscore/contextpair.h>
#include <oscore/protection.h>
#include <oscore_native/test.h>
#include <oscore/context_impl/primitive.h>

#ifdef ESP_PLATFORM
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_event.h"
#include "nvs_flash.h"
#endif

static const uint8_t ciphertext[13] = {0x61, 0x2f, 0x10, 0x92, 0xf1, 0x77, 0x6f, 0x1c, 0x16, 0x68, 0xb3, 0x82, 0x5e};
static const uint8_t cipher_long[514] = {0x61, 0x2f, 0x10, 0x92, 0xf1, 0x7f, 0x27, 0xf7, 0x46, 0xe0, 0x4a, 0x6b, 0x60, 0x0a, 0x3c, 0x4a, 0x07, 0x84, 0x45, 0x40, 0x7e, 0xb5, 0xec, 0xef, 0x96, 0x96, 0x6f, 0x95, 0xeb, 0x90, 0xb0, 0x2b, 0xae, 0x9c, 0xe7, 0x3b, 0x39, 0x37, 0x3c, 0x03, 0x3d, 0x1c, 0x28, 0xff, 0xb7, 0x9a, 0x77, 0x09, 0x2b, 0x57, 0x34, 0xb9, 0x2c, 0x6b, 0x50, 0x49, 0xcb, 0x04, 0xa8, 0x38, 0x96, 0x5a, 0x5e, 0x51, 0xfc, 0x1b, 0x00, 0x53, 0xaf, 0xe3, 0xcd, 0x44, 0xf8, 0x61, 0x66, 0x74, 0xbb, 0x0d, 0x86, 0xaa, 0xd0, 0x75, 0x18, 0x72, 0x68, 0xe1, 0x4f, 0xe7, 0xc2, 0x60, 0x0e, 0x23, 0xa7, 0x48, 0x16, 0xc4, 0xb0, 0x6c, 0xe4, 0x00, 0xee, 0x86, 0x42, 0x96, 0x3d, 0x94, 0x1d, 0x78, 0x1d, 0xf5, 0x4f, 0xed, 0x78, 0xef, 0xb0, 0xa7, 0xa8, 0xf7, 0x3a, 0x29, 0xc6, 0x3a, 0x82, 0x5b, 0xaf, 0x05, 0xbf, 0x3d, 0x08, 0xc7, 0x2a, 0xd4, 0x01, 0xda, 0xd1, 0xd0, 0x48, 0xbb, 0x17, 0xa9, 0x93, 0xe2, 0x36, 0x3d, 0xf0, 0x7e, 0x34, 0x93, 0x09, 0xa2, 0xa2, 0xea, 0xb8, 0x32, 0xe4, 0xb2, 0x8e, 0x56, 0xa9, 0xca, 0xa6, 0x87, 0xf7, 0x49, 0xc4, 0xce, 0x82, 0x98, 0x0d, 0x35, 0x3b, 0xc4, 0x7c, 0x15, 0xee, 0xcb, 0x50, 0x10, 0x89, 0xca, 0xef, 0x31, 0xf9, 0xd1, 0xb1, 0x90, 0x92, 0x3e, 0x9c, 0x3a, 0x92, 0xbc, 0x77, 0x85, 0xf2, 0x09, 0xc6, 0x98, 0x82, 0x93, 0x9f, 0x51, 0x6b, 0x73, 0xc4, 0x95, 0x6e, 0xec, 0x2b, 0x32, 0x73, 0x7a, 0x53, 0xd0, 0xff, 0x02, 0xc5, 0x22, 0x9b, 0x86, 0xdf, 0x51, 0x68, 0x2b, 0x45, 0x49, 0xbd, 0xba, 0x24, 0xf0, 0x8d, 0x82, 0xff, 0xd1, 0x59, 0x10, 0x75, 0x3d, 0x00, 0x53, 0x6b, 0x90, 0x9d, 0x24, 0xca, 0x2c, 0xee, 0xb2, 0xf2, 0x72, 0xd3, 0xc1, 0x36, 0x97, 0xe6, 0x5d, 0xf9, 0xee, 0x42, 0x15, 0x68, 0x5a, 0x0a, 0x14, 0x21, 0x5e, 0x78, 0x2f, 0x51, 0x7f, 0x28, 0xf5, 0xc4, 0xdc, 0x49, 0xaf, 0xbe, 0x98, 0x5f, 0xf2, 0x7a, 0xf6, 0xeb, 0xdf, 0x4a, 0xfd, 0x95, 0x90, 0x86, 0x89, 0xb8, 0xa4, 0x19, 0xce, 0x82, 0x38, 0xc2, 0x31, 0x29, 0x46, 0xd0, 0x0a, 0x71, 0x71, 0x54, 0x69, 0xb0, 0x3c, 0xb1, 0x2c, 0xbf, 0x0b, 0xe7, 0x2a, 0xc0, 0x8c, 0x0f, 0x3c, 0xf8, 0x0d, 0xd6, 0x08, 0x19, 0x48, 0xba, 0x74, 0x3b, 0xaa, 0x6e, 0xd9, 0x38, 0x72, 0x76, 0x01, 0x1c, 0xd9, 0xb4, 0x45, 0x21, 0x71, 0x2b, 0xfb, 0x14, 0xaa, 0xce, 0xf2, 0x4f, 0xa4, 0xea, 0x1b, 0x83, 0x07, 0xd7, 0xcd, 0x47, 0xbc, 0x2a, 0xc7, 0x0b, 0x79, 0x11, 0x1a, 0xa3, 0x59, 0x7c, 0x1a, 0xf4, 0x40, 0x7a, 0x20, 0x9f, 0x2f, 0xfb, 0x4f, 0xf8, 0xd2, 0xc1, 0xce, 0xde, 0x1a, 0xb9, 0x8f, 0xc3, 0xa7, 0xe8, 0x05, 0x24, 0x0e, 0x93, 0x69, 0xee, 0x92, 0xc6, 0x3f, 0xdc, 0x90, 0x6a, 0xb0, 0xd9, 0x88, 0xa8, 0xb2, 0xf5, 0xe2, 0x49, 0xae, 0x0a, 0x92, 0x90, 0xc1, 0xf0, 0x24, 0x26, 0x80, 0xea, 0xc7, 0x50, 0xb6, 0xcf, 0x9c, 0x82, 0x7c, 0x5a, 0xda, 0xfd, 0xbd, 0xcd, 0x8a, 0xb6, 0x84, 0x99, 0xc7, 0xd2, 0x93, 0x71, 0xd2, 0xd4, 0x51, 0x8a, 0x86, 0xa0, 0x0c, 0x36, 0x7f, 0xc8, 0xd2, 0x8d, 0xda, 0xa1, 0x85, 0x79, 0x13, 0x65, 0xf9, 0xfa, 0x4c, 0x70, 0x98, 0x21, 0xbe, 0x5e, 0xa5, 0xfa, 0xd5, 0x30, 0xc3, 0xe0, 0x3e, 0x80, 0x51, 0x02, 0xdb, 0xfa, 0xb4, 0x9a, 0xc9, 0xd8, 0x83, 0xe1, 0x53, 0x66, 0xd5, 0x0b, 0x8f, 0x4f, 0xfe, 0xb9, 0xf2, 0x62, 0x9a, 0x56, 0x95, 0x31, 0x86, 0x88, 0xc9, 0xfd, 0x20, 0xf9, 0xdc, 0xf6, 0xe4, 0x6c, 0x20, 0x75, 0x1c, 0x91, 0x9d, 0x7f, 0x8c, 0x4b, 0x98, 0x3b, 0x66};
static const char lorem[500] = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ";

int oscore_wrap() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},
            .sender_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                           0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    // Prepare CoAP message
    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(coap_msg, 0x01);
    msgerr = oscore_msg_native_append_option(coap_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(coap_msg, 11, (uint8_t*)"tv1", 3);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_trim_payload(coap_msg, 0);
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Wrap & Protect message
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    enum oscore_protect_request_result result;
    result = oscore_wrap_request(coap_msg, oscore_msg, &secctx);
    assert(result == OSCORE_PROTECT_REQUEST_OK);

    uint8_t *payload;
    size_t payload_len = 0;
    oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(payload_len == sizeof(ciphertext));
    assert(memcmp(payload, ciphertext, sizeof(ciphertext)) == 0);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}


int oscore_wrap_long() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},
            .sender_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                           0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    // Prepare CoAP message
    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(coap_msg, 0x01);
    msgerr = oscore_msg_native_append_option(coap_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(coap_msg, 11, (uint8_t*)"tv1", 3);
    assert(!oscore_msgerr_native_is_error(msgerr));

    uint8_t *_payload;
    size_t _payload_len = 0;
    oscore_msg_native_map_payload(coap_msg, &_payload, &_payload_len);
    memcpy(_payload, lorem, sizeof(lorem));
    msgerr = oscore_msg_native_trim_payload(coap_msg, sizeof(lorem));
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Wrap & Protect message
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    enum oscore_protect_request_result result;
    result = oscore_wrap_request(coap_msg, oscore_msg, &secctx);
    assert(result == OSCORE_PROTECT_REQUEST_OK);

    uint8_t *payload;
    size_t payload_len = 0;
    oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(payload_len == sizeof(cipher_long));
    assert(memcmp(payload, cipher_long, sizeof(cipher_long)) == 0);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}

int oscore_unwrap() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},

            .recipient_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                              0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(oscore_msg, 0x02);
    msgerr = oscore_msg_native_append_option(oscore_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(oscore_msg, OSCORE_OPT_NUM, (uint8_t*)"\x09\x14", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));
    uint8_t *payload;
    size_t payload_len = 0;
    msgerr = oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr) && payload_len > sizeof(ciphertext));
    memcpy(payload, ciphertext, sizeof(ciphertext));
    msgerr = oscore_msg_native_trim_payload(oscore_msg, sizeof(ciphertext));
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Unwrap & Unprotect message
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    enum oscore_unprotect_request_result result;
    result = oscore_unwrap_request(oscore_msg, coap_msg, &secctx);
    assert(result == OSCORE_UNPROTECT_REQUEST_OK);

    msgerr = oscore_msg_native_map_payload(coap_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr));
    assert(payload_len == 0);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}

int oscore_unwrap_long() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},

            .recipient_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                              0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(oscore_msg, 0x02);
    msgerr = oscore_msg_native_append_option(oscore_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(oscore_msg, OSCORE_OPT_NUM, (uint8_t*)"\x09\x14", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));
    uint8_t *payload;
    size_t payload_len = 0;
    msgerr = oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr) && payload_len > sizeof(cipher_long));
    memcpy(payload, cipher_long, sizeof(cipher_long));
    msgerr = oscore_msg_native_trim_payload(oscore_msg, sizeof(cipher_long));
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Unwrap & Unprotect message
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    enum oscore_unprotect_request_result result;
    result = oscore_unwrap_request(oscore_msg, coap_msg, &secctx);
    assert(result == OSCORE_UNPROTECT_REQUEST_OK);

    msgerr = oscore_msg_native_map_payload(coap_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr));
    assert(payload_len == sizeof(lorem));
    assert(memcmp(payload, lorem, sizeof(lorem)) == 0);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}


void oscore_perf(char *desc, int test()) {
    struct timer_results results;
    int res = run_for_duration(test, 10, &results);
    if (res != 0) {
        TEST_FAIL_MESSAGE("Failed to measure performance");
    }
    char msg[200];
    int offset = sprintf(msg, "%s\n", desc);
    format_results(msg + offset, &results);
    TEST_IGNORE_MESSAGE(msg);
}

void oscore_perf_wrap() {
    oscore_perf("Performance for wrapping (protect)", oscore_wrap);
}


void oscore_perf_wrap_long() {
    oscore_perf("Performance for wrapping (protect) long", oscore_wrap_long);
}

void oscore_perf_unwrap() {
    oscore_perf("Performance for unwrapping (unprotect)", oscore_unwrap);
}

void oscore_perf_unwrap_long() {
    oscore_perf("Performance for unwrapping (unprotect) long", oscore_unwrap_long);
}

void run_tests() {
    UNITY_BEGIN();
    RUN_TEST(oscore_perf_wrap);
    RUN_TEST(oscore_perf_wrap_long);
    RUN_TEST(oscore_perf_unwrap);
    RUN_TEST(oscore_perf_unwrap_long);
    UNITY_END();
}

#ifdef ESP_PLATFORM
void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    xTaskCreate(run_tests, "edhoc", 8 * 1024, NULL, 5, NULL);
}
#else
int main() {
    run_tests();
}
#endif
