#include <debug.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <cbor.h>

#define INDENT 2

enum cbor_type {
    unknown,
    array,
    map_label,
    map_value
};

static size_t printcbor(size_t depth, bool internal, enum cbor_type type, size_t elems, const uint8_t *buf, size_t len);

void dump(char *title, const uint8_t *buf, size_t len) {
    if (title != NULL) {
        printf("%s:\n", title);
    }
    printf("0x");
    for (size_t i = 0; i < len; ++i) {
        printf("%02x", buf[i]);
    }
    printf("\n");
}

void dumpdiag(char *title, const uint8_t* buf, size_t len) {
    int printable = 1;
    for (size_t i = 0; i < len; ++i) {
        if (!isprint(buf[i])) {
            printable = 0;
        }
    }

    if (printable) {
        if (title != NULL) {
            printf("%s:\n", title);
        }
        printf("%.*s\n", (int)len, buf);
    } else {
        dump(title, buf, len);
    }
}

void dumpcbor(char *title, const uint8_t *buf, size_t len) {
    if (title != NULL) {
        printf("%s:\n", title);
    }
    printf("CBOR <\n");
    printcbor(INDENT, false, unknown, 0, buf, len);
    printf(">\n");
}

void dumpcborx(char *title, const uint8_t *buf, size_t len) {
    if (title != NULL) {
        printf("%s:\n", title);
    }
    printf("CBOR <\n");
    printcbor(INDENT, true, unknown, 0, buf, len);
    printf(">\n");
}

void indent(int indent) {
    printf("%*s", indent, "");
}

static int validatecbor(enum cbor_type type, const uint8_t *buf, size_t len) {
    if (len == 0) {
        return -1;
    }
    if (*buf == 0xFF) {
        return 1;
    }

    size_t output = 0;
    enum edhoc_cbor_major major;
    const uint8_t *p = buf;
    size_t hdr = edhoc_cbor_intdecode(buf, &output, &major);
    p += hdr;

    if (hdr > len) {
        return -1;
    }

    int inner = 0;
    switch (major) {
        case CBOR_MAJOR_UINT:
        case CBOR_MAJOR_NINT:
        case CBOR_MAJOR_TAG:
            if (hdr == 0) {
                return -1;
            }
            break;
        case CBOR_MAJOR_BSTR:
        case CBOR_MAJOR_TSTR:
            p += output;
            break;
        case CBOR_MAJOR_ARRY:
            inner = validatecbor(array, p, len - hdr);
            if (inner < 0) {
                return -1;
            }
            p += inner;
            break;
        case CBOR_MAJOR_MAP:
            inner = validatecbor(map_label, p, len - hdr);
            if (inner < 0) {
                return -1;
            }
            p += inner;
            break;
        case CBOR_MAJOR_PRIM:
            if (hdr == 1 && (output < 20 || output > 23)) {
                return -1;
            }
            break;
        default:
            return -1;
    }

    size_t treated = p - buf;
    if (treated < len) {
        if (type == map_label) {
            type = map_value;
        } else if (type == map_value) {
            type = map_label;
        }
        int res = validatecbor(type, p, len - treated);
        return res < 0 ? -1 : (int)treated + res;
    } else if (type == map_label) {
        return -1;
    }
    return len;
}

static size_t printcbor(size_t depth, bool internal, enum cbor_type type,
        size_t elems, const uint8_t *buf, size_t len) {
    if (*buf == 0xFF) {
        indent(depth);
        printf("END[?]\n");
        return 1;
    }

    size_t output = 0;
    enum edhoc_cbor_major major;
    const uint8_t *p = buf;
    size_t hdr = edhoc_cbor_intdecode(buf, &output, &major);
    bool indefinite = hdr == 0;
    hdr = hdr > 0 ? hdr : 1;
    p += hdr;

    if (hdr > len) {
        indent(depth);
        printf("Buffer overrun\n");
        return len;
    }

    if (type != map_value) {
        indent(depth);
    }
    switch (major) {
        case CBOR_MAJOR_UINT:
            if (indefinite) {
                printf("Unknown CBOR: 0x%02x major: 0x%02x\n", *buf, major);
                return len;
            }
            printf("%zu", output);
            break;
        case CBOR_MAJOR_NINT:
            if (indefinite) {
                printf("Unknown CBOR: 0x%02x major: 0x%02x\n", *buf, major);
                return len;
            }
            printf("%ld", -1 - (long int)output);
            break;
        case CBOR_MAJOR_BSTR:
            if (indefinite) {
                printf("BSTR[?]");
                break;
            }
            printf("BSTR[%d]: 0x", (int)output);
            for (size_t i = 0; i < output; ++i) {
                printf("%02x", p[i]);
            }
            if (internal && output > 0 && validatecbor(unknown, p, output) == output) {
                printf("\n");
                indent(depth);
                printf("CBOR? <\n");
                printcbor(depth + INDENT, internal, unknown, 0, p, output);
                indent(depth);
                printf(">");
            }
            p += output;
            break;
        case CBOR_MAJOR_TSTR:
            if (indefinite) {
                printf("TSTR[?]");
                break;
            }
            printf("TSTR[%d]: \"%*.*s\"", (int)output, (int)output, (int)output, p);
            p += output;
            break;
        case CBOR_MAJOR_ARRY:
            if (indefinite) {
                printf("Array[?]: [\n");
            } else {
                printf("Array[%d]: [\n", (int)output);
            }
            if (output > 0 || indefinite) {
                p += printcbor(depth + INDENT, internal, array, output, p, len - hdr);
            }
            indent(depth);
            printf("]");
            break;
        case CBOR_MAJOR_MAP:
            if (indefinite) {
                printf("Map[?]: {\n");
            } else {
                printf("Map[%d]: {\n", (int)output);
            }
            if (output > 0 || indefinite) {
                p += printcbor(depth + INDENT, internal, map_label, output, p, len - hdr);
            }
            indent(depth);
            printf("}");
            break;
        case CBOR_MAJOR_TAG:
            if (hdr == 0) {
                printf("Unknown CBOR: 0x%02x major: 0x%02x\n", *buf, major);
                return len;
            }
            printf("TAG: %zu", output);
            break;
        case CBOR_MAJOR_PRIM:
            switch (hdr) {
                case 0:
                    printf("Unknown CBOR: 0x%02x major: 0x%02x\n", *buf, major);
                    return len;
                case 1: {
                    switch (output) {
                        case 20:
                            printf("False");
                            break;
                        case 21:
                            printf("True");
                            break;
                        case 22:
                            printf("Null");
                            break;
                        case 23:
                            printf("Undefined");
                            break;
                        default:
                            printf("Unknown CBOR: 0x%02x major: 0x%02x\n", *buf, major);
                            return len;
                    }
                    break;
                }
                case 2:
                    printf("0x%02x", (uint8_t)output);
                    break;
                case 3:
                    printf("half-float - tbd");
                    break;
                case 5:
                    printf("float - tbd");
                    break;
                default:
                    printf("WTF");
                    break;
            }
            break;
        default:
            printf("Unknown CBOR: 0x%02x major: 0x%02x\n", *buf, major);
            return len;
    }

    size_t treated = p - buf;
    if (treated < len) {
        if (type == map_label) {
            type = map_value;
            printf(" => ");
        } else {
            if (type == map_value) {
                type = map_label;
            }
            printf(",\n");
        }
        if (elems > 0) {
            if (elems == 1) {
                return treated;
            }
            elems--;
        }
        return treated + printcbor(depth, internal, type, elems, p, len - treated);
    } else {
        printf("\n");
    }
    return len;
}
