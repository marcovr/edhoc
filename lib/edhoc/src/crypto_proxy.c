#include <crypto_proxy.h>
#include <cose/crypto.h>

edhoc_cryptoerr_t edhoc_crypto_hash_init(
        edhoc_crypto_hashstate_t *state,
        edhoc_crypto_hashalg_t alg
) {
#ifdef CRYPTO_SODIUM
    if (alg != COSE_ALGO_SHA256) {
        return COSE_ERR_NOTIMPLEMENTED;
    }
    return crypto_hash_sha256_init(state);
#else
    return COSE_ERR_NOTIMPLEMENTED;
#endif
}

edhoc_cryptoerr_t edhoc_crypto_hash_update(
        edhoc_crypto_hashstate_t *state,
        const uint8_t *input,
        size_t length
) {
#ifdef CRYPTO_SODIUM
    return crypto_hash_sha256_update(state, input, length);
#else
    return COSE_ERR_NOTIMPLEMENTED;
#endif
}

edhoc_cryptoerr_t edhoc_crypto_hash_final(
        edhoc_crypto_hashstate_t *state,
        uint8_t *hash
) {
#ifdef CRYPTO_SODIUM
    return crypto_hash_sha256_final(state, hash);
#else
    return COSE_ERR_NOTIMPLEMENTED;
#endif
}

edhoc_cryptoerr_t edhoc_crypto_sign(
        edhoc_crypto_curvealg_t alg,
        uint8_t *input,
        size_t input_len,
        const uint8_t *privkey,
        const uint8_t *pubkey,
        uint8_t *signature,
        size_t *signature_len
) {
    cose_curve_t curve;
    switch (alg) {
        case COSE_ALGO_EDDSA:
            curve = COSE_EC_CURVE_ED25519;
            break;
        case COSE_ALGO_ES256:
            curve = COSE_EC_CURVE_P256;
            break;
        default:
            return COSE_ERR_NOTIMPLEMENTED;
    }
    cose_key_t key = {
            .crv = curve,
            .algo = alg,
            .x = (uint8_t *)pubkey,
            .d = (uint8_t *)privkey
    };
    return cose_crypto_sign(&key, signature, signature_len, input, input_len);
}

edhoc_cryptoerr_t edhoc_crypto_sign_verify(
        edhoc_crypto_curvealg_t alg,
        uint8_t *input,
        size_t input_len,
        const uint8_t *pubkey,
        const uint8_t *signature,
        size_t signature_len
) {
    cose_curve_t curve;
    switch (alg) {
        case COSE_ALGO_EDDSA:
            curve = COSE_EC_CURVE_ED25519;
            break;
        case COSE_ALGO_ES256:
            curve = COSE_EC_CURVE_P256;
            break;
        default:
            return COSE_ERR_NOTIMPLEMENTED;
    }
    cose_key_t key = {
            .crv = curve,
            .algo = alg,
            .x = (uint8_t *)pubkey
    };
    return cose_crypto_verify(&key, signature, signature_len, input, input_len);
}

edhoc_cryptoerr_t edhoc_crypto_shared_secret(
        edhoc_crypto_curvealg_t alg,
        uint8_t *secret,
        const uint8_t *privkey,
        const uint8_t *pubkey
) {
#ifdef CRYPTO_SODIUM
    if (alg != COSE_ALGO_EDDSA) {
        return COSE_ERR_NOTIMPLEMENTED;
    }
    return crypto_scalarmult_curve25519(secret, privkey, pubkey);
#else
    return COSE_ERR_NOTIMPLEMENTED;
#endif
}

edhoc_cryptoerr_t edhoc_crypto_make_keypair(
        edhoc_crypto_curvealg_t alg,
        uint8_t *coord_x,
        uint8_t *private
) {
#ifdef CRYPTO_SODIUM
    if (alg != COSE_ALGO_EDDSA) {
        return COSE_ERR_NOTIMPLEMENTED;
    }
    randombytes_buf(private, crypto_scalarmult_curve25519_BYTES);
    crypto_scalarmult_curve25519_base(coord_x, private);
    return COSE_OK;
#else
    return COSE_ERR_NOTIMPLEMENTED;
#endif
}

edhoc_cryptoerr_t edhoc_crypto_hash_from_hmac_number(
        edhoc_crypto_hashalg_t *alg,
        int32_t number
) {
    if (number == COSE_ALGO_HMAC256) {
        *alg = COSE_ALGO_SHA256;
        return COSE_OK;
    }
    return COSE_ERR_NOTIMPLEMENTED;
}

edhoc_cryptoerr_t edhoc_crypto_curvealg_from_number(
        edhoc_crypto_curvealg_t *alg,
        int32_t number
) {
    *alg = number;
    return COSE_OK;
}

edhoc_cryptoerr_t edhoc_crypto_hkdf_from_number(
        edhoc_crypto_curvealg_t *alg,
        int32_t number
) {
    *alg = number;
    return COSE_OK;
}

size_t edhoc_crypto_curvealg_get_signlength(edhoc_crypto_curvealg_t alg) {
    cose_key_t key = {
            .algo = alg
    };
    return cose_crypto_sig_size(&key);
}

bool edhoc_crypto_validate_coord(edhoc_crypto_curvealg_t alg, const uint8_t *coord_x) {
    (void)coord_x;
    if (alg != COSE_ALGO_EDDSA) {
        return false;
    }
    // X25519 keys are designed to be always valid
    return true;
}
