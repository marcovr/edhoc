#include <edhoc.h>
#include <cbor.h>
#include <oscore_native/msg_type.h>
#include <string.h>

/** @brief Convenience method to read a byte string encoded for CBOR
 *
 * Reads CBOR BSTR into a target buffer and advances the source buffer pointer.
 * It is both checked whether the BSTR overruns the source or target buffer,
 * both cases resulting in an error.
 *
 * @param[inout] buffer Source buffer pointer (will be advanced)
 * @param[in] buffer_end End of source buffer
 * @param[out] target Target buffer where the BSTR is written to
 * @param[in] target_size Length of @param target
 */
static edhoc_error_t read_cbor_bstr(
        uint8_t **buffer,
        const uint8_t *buffer_end,
        uint8_t *target,
        size_t *target_size
) {
    size_t length;
    enum edhoc_cbor_major major;
    *buffer += edhoc_cbor_intdecode(*buffer, &length, &major);
    if (major != CBOR_MAJOR_BSTR || *buffer + length > buffer_end) {
        return EDHOC_ERROR_INVALID_MSG;
    }
    if (length > *target_size) {
        return EDHOC_ERROR_BUFFER_TOO_SHORT;
    }
    *target_size = length;
    memcpy(target, *buffer, length);
    *buffer += length;
    return EDHOC_OK;
}

/** @brief Convenience method to read an EDHOC key encoded for CBOR
 *
 * This method is analogous to @ref read_cbor_bstr; but with the value having
 * to be exactly @ref EDHOC_KEYSIZE; bytes long.
 *
 * @param[inout] buffer Source buffer pointer (will be advanced)
 * @param[in] buffer_end End of source buffer
 * @param[out] target Target buffer where the BSTR is written to
 */
static edhoc_error_t read_cbor_key(uint8_t **buffer, uint8_t *buffer_end, uint8_t *target) {
    size_t target_size = EDHOC_KEYSIZE;
    edhoc_error_t err;
    err = read_cbor_bstr(buffer, buffer_end, target, &target_size);
    if (err != EDHOC_OK) {
        return err;
    }
    return target_size == EDHOC_KEYSIZE ? EDHOC_OK : EDHOC_ERROR_INVALID_KEYSIZE;
}

edhoc_error_t edhoc_init_cipher_suite(int8_t suite_number, edhoc_cipher_suite_t *suite) {
    switch (suite_number) {
        case 0:
            // (AES-CCM-64-64-128, HMAC 256/256, X25519, EdDSA, Ed25519)
            suite->algs[0] = 10;
            suite->algs[1] = 5;
            suite->algs[2] = 4;
            suite->algs[3] = -8;
            break;
        case 1:
            // (AES-CCM-64-64-128, HMAC 256/256, P-256, ES256, P-256)
            suite->algs[0] = 10;
            suite->algs[1] = 5;
            suite->algs[2] = 1;
            suite->algs[3] = -7;
            break;
        default:
            return EDHOC_ERROR_UNSUPPORTED_SUITE;
    }
    suite->number = suite_number;
    return EDHOC_OK;
}

/** @brief Write a COSE Sign1 structure into a buffer
 *
 * The output buffer needs to be large enough for the structure to fit into.
 *
 * @param[out] buffer Output buffer where the structure is written to
 * @param[in] transcript_hash Current transcript hash to use in the structure
 * @param[in] key Key information to use in the structure
 */
static void edhoc_build_sign_structure(
        uint8_t *buffer,
        const uint8_t *transcript_hash,
        edhoc_auth_keypair_t *key
) {
    memcpy(buffer, "\x84\x6ASignature1", 12);
    uint8_t *p = buffer + 12;

    // ID_CRED_V
    p += edhoc_cbor_intencode(2 + edhoc_cbor_bstrsize(key->kid_len), p, CBOR_MAJOR_BSTR);
    p[0] = 0xa1; // Map with 1 element
    p[1] = 0x04; // KID parameter
    p += 2;
    p += edhoc_cbor_bstrencode(key->kid, key->kid_len, p);

    // CRED_V (COSE_KEY)
    memcpy(p, transcript_hash, EDHOC_THSIZE);
    p += EDHOC_THSIZE;
    p += edhoc_cbor_intencode(6 + edhoc_cbor_bstrsize(EDHOC_KEYSIZE), p, CBOR_MAJOR_BSTR);
    p[0] = 0xa3; // Map with 3 elements
    p[1] = 0x01; // Key type parameter
    p[2] = 0x01; // -> Octet Key Pair
    p[3] = 0x20; // EC identifier parameter
    p[4] = 0x06; // -> Ed25519
    p[5] = 0x21; // X-Coord parameter
    p += 6;
    edhoc_cbor_bstrencode(key->public, EDHOC_KEYSIZE, p);
}

/** @brief Produce a signature
 *
 * The output buffer needs to be large enough for the signature to fit into.
 *
 * @param[in] ctx EDHOC context to use
 * @param[in] transcript_hash Current transcript hash for the signature
 * @param[out] signature Output buffer where the signature is written to
 */
static edhoc_error_t edhoc_sign(
        edhoc_context_t *ctx,
        const uint8_t *transcript_hash,
        uint8_t *signature,
        size_t *signature_len
) {
    size_t bufsize = 12 +
                     edhoc_cbor_bstrsize(2 + edhoc_cbor_bstrsize(ctx->auth_keypair->kid_len)) +
                     EDHOC_THSIZE +
                     edhoc_cbor_bstrsize(6 + edhoc_cbor_bstrsize(EDHOC_KEYSIZE));
    uint8_t buffer[bufsize];
    edhoc_build_sign_structure(buffer, transcript_hash, ctx->auth_keypair);

    edhoc_cryptoerr_t err;
    edhoc_crypto_curvealg_t alg;
    edhoc_crypto_curvealg_from_number(&alg, ctx->suite.algs[3]);
    err = edhoc_crypto_sign(
            alg,
            buffer,
            bufsize,
            ctx->auth_keypair->private,
            ctx->auth_keypair->public,
            signature,
            signature_len
    );

    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

/** @brief Verify a signature
 *
 * If the method succeeds (EDHOC_ERROR_OK), then the signature is valid.
 *
 * @param[in] ctx EDHOC context to use
 * @param[in] transcript_hash Current transcript hash for the signature
 * @param[in] signature Signature to verify
 */
static edhoc_error_t edhoc_sign_verify(
        edhoc_context_t *ctx,
        const uint8_t *transcript_hash,
        const uint8_t *signature,
        size_t signature_len
) {
    size_t bufsize = 12 +
                     edhoc_cbor_bstrsize(2 + edhoc_cbor_bstrsize(ctx->peer.auth_keypair->kid_len)) +
                     EDHOC_THSIZE +
                     edhoc_cbor_bstrsize(6 + edhoc_cbor_bstrsize(EDHOC_KEYSIZE));
    uint8_t buffer[bufsize];
    edhoc_build_sign_structure(buffer, transcript_hash, ctx->peer.auth_keypair);

    edhoc_cryptoerr_t err;
    edhoc_crypto_curvealg_t alg;
    edhoc_crypto_curvealg_from_number(&alg, ctx->suite.algs[3]);
    err = edhoc_crypto_sign_verify(
            alg,
            buffer,
            bufsize,
            ctx->peer.auth_keypair->public,
            signature,
            signature_len
    );

    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

/** Calculate the ciphertext length */
static size_t edhoc_determine_ciphertext_length(edhoc_context_t *ctx, size_t signature_len) {
    edhoc_crypto_aeadalg_t alg;
    edhoc_crypto_aead_from_number(&alg, ctx->suite.algs[0]);
    return edhoc_cbor_bstrsize(ctx->auth_keypair->kid_len) +
           edhoc_cbor_bstrsize(signature_len) +
            edhoc_crypto_aead_get_taglength(alg);
    // todo: add support for optional application data
}

/** Get the maximal signature length */
static size_t edhoc_max_signature_length(edhoc_context_t *ctx) {
    edhoc_crypto_curvealg_t curve_alg;
    edhoc_crypto_curvealg_from_number(&curve_alg, ctx->suite.algs[3]);
    return edhoc_crypto_curvealg_get_signlength(curve_alg);
}

edhoc_error_t edhoc_transcript_hash_start(
        edhoc_context_t *ctx,
        const uint8_t *data1,
        size_t data1_len,
        const uint8_t *data2,
        size_t data2_len
) {
    edhoc_crypto_hashalg_t hash_alg;
    edhoc_crypto_hash_from_hmac_number(&hash_alg, ctx->suite.algs[1]);
    edhoc_crypto_hashstate_t *state = &ctx->transcript_hash_state;
    edhoc_cryptoerr_t err;
    err = edhoc_crypto_hash_init(state, hash_alg);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    err = edhoc_crypto_hash_update(state, data1, data1_len);
    if (data2 != NULL) {
        if (edhoc_cryptoerr_is_error(err)) {
            return EDHOC_ERROR_CRYPTO_FAILED;
        }
        err = edhoc_crypto_hash_update(state, data2, data2_len);
    }

    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

edhoc_error_t edhoc_transcript_hash_final(
        edhoc_context_t *ctx,
        const uint8_t *data,
        size_t data_len,
        uint8_t transcript_hash[EDHOC_THSIZE]
) {
    edhoc_crypto_hashstate_t *state = &ctx->transcript_hash_state;
    edhoc_cryptoerr_t err;
    if (data != NULL) {
        err = edhoc_crypto_hash_update(state, data, data_len);
        if (edhoc_cryptoerr_is_error(err)) {
            return EDHOC_ERROR_CRYPTO_FAILED;
        }
    }
    transcript_hash += edhoc_cbor_intencode(EDHOC_HASHSIZE, transcript_hash, CBOR_MAJOR_BSTR);
    err = edhoc_crypto_hash_final(state, transcript_hash);
    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

/** @brief Derive a new key and initialization vector for en/decryption
 *
 * The output buffers need to be large enough for the parameters to fit inside.
 *
 * @param[in] ctx EDHOC context to use
 * @param[in] aead_alg AEAD algorithm to be used
 * @param[in] transcript_hash Current transcript hash for derivation
 * @param[out] key Output buffer where the key is written to
 * @param[out] iv Output buffer where the iv is written to
 */
static edhoc_error_t edhoc_derive_encryption_params(
        edhoc_context_t *ctx,
        edhoc_crypto_aeadalg_t aead_alg,
        const uint8_t *transcript_hash,
        uint8_t *key,
        uint8_t *iv
) {
    // todo: less hardcoded values

    uint8_t secret[EDHOC_KEYSIZE];
    edhoc_crypto_curvealg_t curve_alg;
    edhoc_crypto_curvealg_from_number(&curve_alg, ctx->suite.algs[3]);
    edhoc_cryptoerr_t err;
    err = edhoc_crypto_shared_secret(curve_alg, secret, ctx->ecdh_keypair.private, ctx->peer.ecdh_coord_x);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_INVALID_COORD;
    }

    size_t key_len = EDHOC_AEAD_KEYSIZE;
    size_t iv_len = edhoc_crypto_aead_get_ivlength(aead_alg);
    uint8_t info_iv_len = 27 + EDHOC_THSIZE;
    uint8_t info_key_len = info_iv_len - 13;
    uint8_t info_iv[info_iv_len];
    uint8_t *info_key = info_iv + 13;
    memcpy(info_key, "\x84\x0a\x83\xf6\xf6\xf6\x83\xf6\xf6\xf6\x83\x18\x80\x40", 14);
    memcpy(info_key + 14, transcript_hash, EDHOC_THSIZE);

    edhoc_crypto_hkdfalg_t alg;
    edhoc_crypto_hkdf_from_number(&alg, ctx->suite.algs[1]);
    err = edhoc_crypto_hkdf_derive(
            alg,
            (uint8_t *)"",
            0,
            secret,
            EDHOC_KEYSIZE,
            info_key,
            info_key_len,
            key,
            key_len
    );
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    memcpy(info_iv, "\x84\x6DIV-GENERATION", 15);
    info_iv[25] = 0x68;
    err = edhoc_crypto_hkdf_derive(
            alg,
            (uint8_t *)"",
            0,
            secret,
            EDHOC_KEYSIZE,
            info_iv,
            info_iv_len,
            iv,
            iv_len
    );
    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

/** @brief Write the EDHOC ciphertext into the message buffer
 *
 * The message buffer needs to be large enough for the ciphertext to fit into.
 *
 * @param[in] ctx EDHOC context to use
 * @param[in] transcript_hash Current transcript hash
 * @param[out] buffer Message buffer where the ciphertext is written to
 * @param[in] ciphertext_len Length of the ciphertext to produce
 */
static edhoc_error_t edhoc_write_ciphertext(
        edhoc_context_t *ctx,
        const uint8_t *transcript_hash,
        const uint8_t *signature,
        size_t signature_len,
        uint8_t *buffer,
        size_t ciphertext_len
) {
    edhoc_crypto_aeadalg_t alg;
    edhoc_crypto_aead_from_number(&alg, ctx->suite.algs[0]);
    edhoc_error_t e_err;
    uint8_t key[EDHOC_AEAD_KEYSIZE];
    uint8_t iv[edhoc_crypto_aead_get_ivlength(alg)];
    e_err = edhoc_derive_encryption_params(ctx, alg, transcript_hash, key, iv);
    if (e_err != EDHOC_OK) {
        return e_err;
    }

    uint8_t *p = buffer;
    p += edhoc_cbor_bstrencode(ctx->auth_keypair->kid, ctx->auth_keypair->kid_len, p);
    p += edhoc_cbor_bstrencode(signature, signature_len, p);
    // todo: add support for optional application data

    size_t aad_len = 11 + EDHOC_THSIZE;
    uint8_t aad[aad_len];
    memcpy(aad, "\x83\x68""Encrypt0\x40", 11);
    memcpy(aad + 11, transcript_hash, EDHOC_THSIZE);

    edhoc_crypto_aead_encryptstate_t state;
    size_t plaintext_len = p - buffer;
    edhoc_cryptoerr_t err;
    err = edhoc_crypto_aead_encrypt_start(&state, alg, aad_len, plaintext_len, iv, key);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    err = edhoc_crypto_aead_encrypt_feed_aad(&state, aad, aad_len);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    err = edhoc_crypto_aead_encrypt_inplace(&state, buffer, ciphertext_len);

    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

/** Find a public key of another party by kid */
static edhoc_error_t edhoc_find_pubkey(edhoc_context_t *ctx, uint8_t *kid, size_t kid_len) {
    for (size_t i = 0; i < ctx->peer_keys_len; ++i) {
        if (kid_len == ctx->peer_keys[i]->kid_len &&
            memcmp(kid, ctx->peer_keys[i]->kid, kid_len) == 0) {
            ctx->peer.auth_keypair = ctx->peer_keys[i];
            return EDHOC_OK;
        }
    }
    return EDHOC_ERROR_KEY_NOT_FOUND;
}

/** Read the EDHOC ciphertext from the message buffer
 *
 * The message buffer is modified since the ciphertext is decrypted in place.
 * Afterwards, the decrypted signature is verified.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[in] transcript_hash Current transcript hash
 * @param[inout] buffer Message buffer where the ciphertext is decrypted
 * @param[in] ciphertext_len Length of the ciphertext to produce
 */
static edhoc_error_t edhoc_read_ciphertext(
        edhoc_context_t *ctx,
        const uint8_t *transcript_hash,
        uint8_t *buffer,
        size_t ciphertext_len
) {
    edhoc_crypto_aeadalg_t alg;
    edhoc_crypto_aead_from_number(&alg, ctx->suite.algs[0]);
    uint8_t key[EDHOC_AEAD_KEYSIZE];
    uint8_t iv[edhoc_crypto_aead_get_ivlength(alg)];
    edhoc_error_t e_err;
    e_err = edhoc_derive_encryption_params(ctx, alg, transcript_hash, key, iv);
    if (e_err != EDHOC_OK) {
        return e_err;
    }

    size_t aad_len = 11 + EDHOC_THSIZE;
    uint8_t aad[aad_len];
    memcpy(aad, "\x83\x68""Encrypt0\x40", 11);
    memcpy(aad + 11, transcript_hash, EDHOC_THSIZE);

    edhoc_crypto_aead_decryptstate_t state;
    size_t plaintext_len = ciphertext_len - edhoc_crypto_aead_get_taglength(alg);
    uint8_t *plaintext_end = buffer + plaintext_len;
    edhoc_cryptoerr_t err;
    err = edhoc_crypto_aead_decrypt_start(&state, alg, aad_len, plaintext_len, iv, key);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    err = edhoc_crypto_aead_decrypt_feed_aad(&state, aad, aad_len);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    err = edhoc_crypto_aead_decrypt_inplace(&state, buffer, ciphertext_len);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }

    size_t output;
    enum edhoc_cbor_major major;
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major != CBOR_MAJOR_BSTR || output >= plaintext_len) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    e_err = edhoc_find_pubkey(ctx, buffer, output);
    if (e_err != EDHOC_OK) {
        return e_err;
    }

    buffer += output;
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major != CBOR_MAJOR_BSTR || buffer + output != plaintext_end) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }

    return edhoc_sign_verify(ctx, transcript_hash, buffer, output);
}

edhoc_error_t edhoc_write_message_err(
        edhoc_context_t *ctx,
        edhoc_error_t error,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
) {
    size_t err_msg_len = 0;
    char *err_msg;

    switch (error) {
        case EDHOC_OK:
        case EDHOC_ERROR_IS_ERROR_MSG:
            // No need to send an error message.
            // Either no error at all or an error message was received.
            return error;
        case EDHOC_ERROR_INVALID_MSG:
            err_msg = "Malformed message";
            break;
        case EDHOC_ERROR_INVALID_KEYSIZE:
            err_msg = "Unsupported keysize";
            break;
        case EDHOC_ERROR_INVALID_COORD:
            err_msg = "X coordinate invalid";
            break;
        case EDHOC_ERROR_UNSUPPORTED_METHOD:
            err_msg = "Only asymmetric method supported";
            break;
        case EDHOC_ERROR_UNSUPPORTED_SUITE:
            err_msg = "Unsupported cipher suite";
            break;
        case EDHOC_ERROR_KEY_NOT_FOUND:
            err_msg = "Public key not found";
            break;
        case EDHOC_ERROR_CTX_NOT_FOUND:
            err_msg = "CID unknown";
            break;
        case EDHOC_ERROR_CID_TOO_LONG:
            err_msg = "CID too long, max 8 bytes";
            break;
        case EDHOC_ERROR_OTHER:
            err_msg = "Unknown error";
            break;
        default:
            err_msg = "Internal error";
            break;
    }

    err_msg_len = strlen(err_msg);
    bool include_peer_conn_id = ctx->party_u ? ctx->type % 4 < 2 : ctx->type % 2 == 0;
    bool include_suites = error == EDHOC_ERROR_UNSUPPORTED_SUITE;

    size_t total_len = edhoc_cbor_bstrsize(err_msg_len);
    if (include_peer_conn_id) {
        total_len += edhoc_cbor_bstrsize(ctx->peer.conn_id_len);
    }
    if (include_suites) {
        // todo: add support for suite array (without index)
        total_len += edhoc_cbor_intsize(ctx->suite.number);
    }
    if (buffer_len < total_len) {
        return EDHOC_ERROR_ERR_MSG_BUF_TOO_SHORT;
    }

    if (include_peer_conn_id) {
        buffer += edhoc_cbor_bstrencode(ctx->peer.conn_id, ctx->peer.conn_id_len, buffer);
    }
    buffer += edhoc_cbor_intencode(err_msg_len, buffer, CBOR_MAJOR_TSTR);
    memcpy(buffer, err_msg, err_msg_len);
    buffer += err_msg_len;
    if (include_suites) {
        edhoc_cbor_intencode(ctx->suite.number, buffer, CBOR_MAJOR_UINT);
    }

    ctx->msg_num = 0;
    *msg_len = total_len;
    return error;
}

edhoc_msg_kind_t edhoc_determine_message_kind(const uint8_t *buffer, size_t buffer_len) {
    const uint8_t *buffer_end = buffer + buffer_len;
    size_t output = 0;
    enum edhoc_cbor_major major;

    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    buffer += output;
    if (major == CBOR_MAJOR_TSTR) {
        return EDHOC_MSG_ERROR;
    } else if (major == CBOR_MAJOR_UINT) {
        return EDHOC_MSG_1;
    } else if (major != CBOR_MAJOR_BSTR || buffer > buffer_end) {
        return EDHOC_MSG_UNKNOWN;
    } else if (buffer == buffer_end) {
        return EDHOC_MSG_3;
    }
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    buffer += output;
    if (major == CBOR_MAJOR_TSTR) {
        return EDHOC_MSG_ERROR;
    } else if (major != CBOR_MAJOR_BSTR || buffer > buffer_end) {
        return EDHOC_MSG_UNKNOWN;
    } else if (buffer == buffer_end) {
        return EDHOC_MSG_3;
    } else {
        return EDHOC_MSG_2;
    }
}

bool edhoc_is_error_msg(const uint8_t *buffer, size_t buffer_len) {
    return edhoc_determine_message_kind(buffer, buffer_len) == EDHOC_MSG_ERROR;
}

edhoc_error_t edhoc_read_message_1(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len
) {
    uint8_t *buffer_start = buffer;
    uint8_t *buffer_end = buffer + buffer_len;
    edhoc_error_t err;
    size_t output;
    enum edhoc_cbor_major major;

    // Check the EDHOC method type
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major != CBOR_MAJOR_UINT || buffer >= buffer_end) {
        return EDHOC_ERROR_INVALID_MSG;
    }
    if (output >= 4 * EDHOC_METHOD_SYMMETRIC) {
        // Only asymmetric method is supported
        return EDHOC_ERROR_UNSUPPORTED_METHOD;
    }
    ctx->type = (uint8_t)output;

    // Check the selected cipher suite
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major != CBOR_MAJOR_UINT || buffer >= buffer_end) {
        // todo: support non-predefined suites (alg arrays)
        // It might also be a suite array
        if (major != CBOR_MAJOR_ARRY ||
            buffer + output >= buffer_end || output < 3) {
            return EDHOC_ERROR_INVALID_MSG;
        }
        size_t length = output - 1;
        size_t index = 0;
        size_t suite = 0;
        // Check the index of the selected suite
        buffer += edhoc_cbor_intdecode(buffer, &index, &major);
        if (major != CBOR_MAJOR_UINT || index > length) {
            return EDHOC_ERROR_INVALID_MSG;
        }
        // Extract the selected suite
        for (size_t i = 0; i < length; ++i) {
            buffer += edhoc_cbor_intdecode(buffer, &suite, &major);
            if (major != CBOR_MAJOR_UINT || buffer >= buffer_end) {
                return EDHOC_ERROR_INVALID_MSG;
            }
            if (i == index) {
                output = suite; // no break, still need to advance cursor
            }
        }
    }
    err = edhoc_init_cipher_suite((int8_t)output, &ctx->suite);
    if (err != EDHOC_OK) {
        return err;
    }

    // Check the ECDH public key of party U
    err = read_cbor_key(&buffer, buffer_end, ctx->peer.ecdh_coord_x);
    if (err != EDHOC_OK) {
        return err;
    }
    edhoc_crypto_curvealg_t alg;
    edhoc_crypto_curvealg_from_number(&alg, ctx->suite.algs[3]);
    if (!edhoc_crypto_validate_coord(alg, ctx->peer.ecdh_coord_x)) {
        return EDHOC_ERROR_INVALID_COORD;
    }

    // Check the connection identifier of party U
    err = read_cbor_bstr(&buffer, buffer_end, ctx->peer.conn_id, &ctx->peer.conn_id_len);
    if (err != EDHOC_OK) {
        return err;
    }
    // todo: add support for optional application data

    // Begin transcript hash 2 with the content of message 1
    return edhoc_transcript_hash_start(ctx, buffer_start, buffer_len, NULL, 0);
}

edhoc_error_t edhoc_read_message_2(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len
) {
    uint8_t *buffer_start = buffer;
    uint8_t *buffer_end = buffer + buffer_len;
    bool include_conn_id = ctx->type % 2 == 0;
    edhoc_error_t err;
    size_t output;
    enum edhoc_cbor_major major;

    // Check the connection identifier if required
    if (include_conn_id) {
        err = read_cbor_bstr(&buffer, buffer_end, ctx->conn_id, &ctx->conn_id_len);
        if (err != EDHOC_OK) {
            return err;
        }
    }

    // Check the ECDH public key of party V
    err = read_cbor_key(&buffer, buffer_end, ctx->peer.ecdh_coord_x);
    if (err != EDHOC_OK) {
        return err;
    }
    edhoc_crypto_curvealg_t alg;
    edhoc_crypto_curvealg_from_number(&alg, ctx->suite.algs[3]);
    if (!edhoc_crypto_validate_coord(alg, ctx->peer.ecdh_coord_x)) {
        return EDHOC_ERROR_INVALID_COORD;
    }

    // Check the connection identifier of party V
    err = read_cbor_bstr(&buffer, buffer_end, ctx->peer.conn_id, &ctx->peer.conn_id_len);
    if (err != EDHOC_OK) {
        return err;
    }

    // Complete transcript hash 2 with the data part of message 2
    uint8_t TH_2[EDHOC_THSIZE];
    err = edhoc_transcript_hash_final(ctx, buffer_start, buffer - buffer_start, TH_2);
    if (err != EDHOC_OK) {
        return err;
    }

    // Begin transcript hash 3 with TH2 and the ciphertext of message 2
    err = edhoc_transcript_hash_start(ctx, TH_2, EDHOC_THSIZE, buffer, buffer_end - buffer);
    if (err != EDHOC_OK) {
        return err;
    }

    // Check the ciphertext
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major != CBOR_MAJOR_UINT && buffer + output != buffer_end) {
        return EDHOC_ERROR_INVALID_MSG;
    }
    return edhoc_read_ciphertext(ctx, TH_2, buffer, buffer_end - buffer);
}

edhoc_error_t edhoc_read_message_3(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len
) {
    uint8_t *buffer_start = buffer;
    uint8_t *buffer_end = buffer + buffer_len;
    bool include_conn_id = ctx->type % 4 < 2;
    edhoc_error_t err;
    size_t output;
    enum edhoc_cbor_major major;

    // Check the connection identifier if required
    if (include_conn_id) {
        err = read_cbor_bstr(&buffer, buffer_end, ctx->conn_id, &ctx->conn_id_len);
        if (err != EDHOC_OK) {
            return err;
        }
    }

    // Complete transcript hash 3 with the data part of message 3
    uint8_t TH_3[EDHOC_THSIZE];
    err = edhoc_transcript_hash_final(ctx, buffer_start, buffer - buffer_start, TH_3);
    if (err != EDHOC_OK) {
        return err;
    }

    // Begin transcript hash 4 with TH3 and the ciphertext of message 3
    err = edhoc_transcript_hash_start(ctx, TH_3, EDHOC_THSIZE, buffer, buffer_end - buffer);
    if (err != EDHOC_OK) {
        return err;
    }

    // Check the ciphertext
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major != CBOR_MAJOR_UINT && buffer + output != buffer_end) {
        return EDHOC_ERROR_INVALID_MSG;
    }
    err = edhoc_read_ciphertext(ctx, TH_3, buffer, buffer_end - buffer);
    if (err != EDHOC_OK) {
        return err;
    }

    ctx->msg_num = 3;
    return EDHOC_OK;
}

edhoc_error_t edhoc_write_message_1(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
) {
    uint8_t *buffer_start = buffer;
    edhoc_error_t err;
    size_t total_len = edhoc_cbor_intsize(ctx->type) +
                       edhoc_cbor_intsize(ctx->suite.number) +
                       edhoc_cbor_bstrsize(EDHOC_KEYSIZE) +
                       edhoc_cbor_bstrsize(ctx->conn_id_len);
    if (buffer_len < total_len) {
        return EDHOC_ERROR_BUFFER_TOO_SHORT;
    }

    buffer += edhoc_cbor_intencode(ctx->type, buffer, CBOR_MAJOR_UINT);
    // todo: add support for suite array
    buffer += edhoc_cbor_intencode(ctx->suite.number, buffer, CBOR_MAJOR_UINT);
    buffer += edhoc_cbor_bstrencode(ctx->ecdh_keypair.coord_x, EDHOC_KEYSIZE, buffer);
    edhoc_cbor_bstrencode(ctx->conn_id, ctx->conn_id_len, buffer);
    // todo: add support for optional application data

    // Begin transcript hash 2 with the content of message 1
    err = edhoc_transcript_hash_start(ctx, buffer_start, total_len, NULL, 0);
    if (err != EDHOC_OK) {
        return err;
    }

    ctx->party_u = true;
    ctx->msg_num = 1;
    *msg_len = total_len;
    return EDHOC_OK;
}

edhoc_error_t edhoc_write_message_2(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
) {
    bool include_peer_conn_id = ctx->type % 2 == 0;
    uint8_t *buffer_start = buffer;
    edhoc_error_t err;

    size_t data_len = edhoc_cbor_bstrsize(EDHOC_KEYSIZE) + edhoc_cbor_bstrsize(ctx->conn_id_len);
    if (include_peer_conn_id) {
        data_len += edhoc_cbor_bstrsize(ctx->peer.conn_id_len);
    }
    if (buffer_len < data_len) {
        return EDHOC_ERROR_BUFFER_TOO_SHORT;
    }

    if (include_peer_conn_id) {
        buffer += edhoc_cbor_bstrencode(ctx->peer.conn_id, ctx->peer.conn_id_len, buffer);
    }
    buffer += edhoc_cbor_bstrencode(ctx->ecdh_keypair.coord_x, EDHOC_KEYSIZE, buffer);
    buffer += edhoc_cbor_bstrencode(ctx->conn_id, ctx->conn_id_len, buffer);

    // Complete transcript hash 2 with the data part of message 2
    uint8_t TH_2[EDHOC_THSIZE];
    err = edhoc_transcript_hash_final(ctx, buffer_start, data_len, TH_2);
    if (err != EDHOC_OK) {
        return err;
    }

    uint8_t signature[edhoc_max_signature_length(ctx)];
    size_t signature_len;
    err = edhoc_sign(ctx, TH_2, signature, &signature_len);
    if (err != EDHOC_OK) {
        return err;
    }
    size_t ciphertext_len = edhoc_determine_ciphertext_length(ctx, signature_len);
    size_t ciphertext_cbor_len = edhoc_cbor_bstrsize(ciphertext_len);
    if (ciphertext_cbor_len > buffer_len - data_len) {
        return EDHOC_ERROR_BUFFER_TOO_SHORT;
    }

    uint8_t *ciphertext = buffer;
    buffer += edhoc_cbor_intencode(ciphertext_len, buffer, CBOR_MAJOR_BSTR);
    err = edhoc_write_ciphertext(ctx, TH_2, signature, signature_len, buffer, ciphertext_len);
    if (err != EDHOC_OK) {
        return err;
    }

    // Begin transcript hash 3 with TH2 and the ciphertext of message 2
    err = edhoc_transcript_hash_start(ctx, TH_2, EDHOC_THSIZE, ciphertext, ciphertext_cbor_len);
    if (err != EDHOC_OK) {
        return err;
    }

    ctx->msg_num = 2;
    *msg_len = data_len + ciphertext_cbor_len;
    return EDHOC_OK;
}

edhoc_error_t edhoc_write_message_3(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
) {
    bool include_peer_conn_id = ctx->type % 4 < 2;
    uint8_t *buffer_start = buffer;
    edhoc_error_t err;

    size_t data_len = 0;
    if (include_peer_conn_id) {
        data_len += edhoc_cbor_bstrsize(ctx->peer.conn_id_len);
    }
    if (buffer_len < data_len) {
        return EDHOC_ERROR_BUFFER_TOO_SHORT;
    }

    if (include_peer_conn_id) {
        buffer += edhoc_cbor_bstrencode(ctx->peer.conn_id, ctx->peer.conn_id_len, buffer);
    }

    // Complete transcript hash 3 with the data part of message 3
    uint8_t TH_3[EDHOC_THSIZE];
    err = edhoc_transcript_hash_final(ctx, buffer_start, data_len, TH_3);
    if (err != EDHOC_OK) {
        return err;
    }

    uint8_t signature[edhoc_max_signature_length(ctx)];
    size_t signature_len;
    err = edhoc_sign(ctx, TH_3, signature, &signature_len);
    if (err != EDHOC_OK) {
        return err;
    }
    size_t ciphertext_len = edhoc_determine_ciphertext_length(ctx, signature_len);
    size_t ciphertext_cbor_len = edhoc_cbor_bstrsize(ciphertext_len);
    if (ciphertext_cbor_len > buffer_len - data_len) {
        return EDHOC_ERROR_BUFFER_TOO_SHORT;
    }

    uint8_t *ciphertext = buffer;
    buffer += edhoc_cbor_intencode(ciphertext_len, buffer, CBOR_MAJOR_BSTR);
    err = edhoc_write_ciphertext(ctx, TH_3, signature, signature_len, buffer, ciphertext_len);
    if (err != EDHOC_OK) {
        return err;
    }

    // Begin transcript hash 4 with TH3 and the ciphertext of message 3
    err = edhoc_transcript_hash_start(ctx, TH_3, EDHOC_THSIZE, ciphertext, ciphertext_cbor_len);
    if (err != EDHOC_OK) {
        return err;
    }

    ctx->msg_num = 3;
    *msg_len = data_len + ciphertext_cbor_len;
    return EDHOC_OK;
}
