#ifndef EDHOC_IMPLEMENTATION_CRYPTO_PROXY_TYPE_H
#define EDHOC_IMPLEMENTATION_CRYPTO_PROXY_TYPE_H

/** @file */

/** @ingroup oscore_extensions
 *  @addtogroup edhoc_crypto_proxy Cryptography API proxy
 *
 * @{
 */

#ifdef CRYPTO_SODIUM
#include <sodium.h>
typedef crypto_hash_sha256_state edhoc_crypto_hashstate_t; /**< Hash state from libsodium */
#else
typedef int edhoc_crypto_hashstate_t; /**< Dummy state used if no Sodium is present */
#endif

typedef oscore_crypto_aeadalg_t edhoc_crypto_aeadalg_t;
typedef oscore_crypto_hkdfalg_t edhoc_crypto_hkdfalg_t;
typedef cose_algo_t edhoc_crypto_hashalg_t;
typedef cose_algo_t edhoc_crypto_curvealg_t;

typedef oscore_crypto_aead_encryptstate_t edhoc_crypto_aead_encryptstate_t;
typedef oscore_crypto_aead_decryptstate_t edhoc_crypto_aead_decryptstate_t;

// Algorithm definitions currently missing in libcose
#define COSE_ALGO_SHA256    (-16) /**< COSE Algorithm number for SHA256 */

typedef oscore_cryptoerr_t edhoc_cryptoerr_t;

/** @} */

#endif //EDHOC_IMPLEMENTATION_CRYPTO_PROXY_TYPE_H
