#ifndef EDHOC_IMPLEMENTATION_EDHOC_H
#define EDHOC_IMPLEMENTATION_EDHOC_H

/** @file */

/** @addtogroup edhoc_api EDHOC API
 *
 * @brief API providing the EDHOC protocol
 *
 * Most methods work directly on message buffers (`uint8_t[]`), thus they can
 * be used with almost any kind of CoAP library.
 *
 * @{
 */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <oscore_native/msg_type.h>
#include <oscore/contextpair.h>
#include <crypto_proxy.h>
#include <edhoc_shared.h>


/** @brief Setup an EDHOC context
 *
 * The keypair needs to be kept in memory for as long as the context is used,
 * since it is not copied but accessed by reference.
 *
 * This method may fail if no ephemeral keys can be generated.
 *
 * @param[out] ctx The EDHOC context to use
 * @param[in] keypair Keypair to use with EDHOC
 * @param[in] peer_keys Keys of known other parties (public & kid only)
 * @param[in] peer_keys_len Length of @p peer_keys
 */
edhoc_error_t edhoc_init_context(
        edhoc_context_t *ctx,
        edhoc_auth_keypair_t *keypair,
        edhoc_auth_keypair_t **peer_keys,
        size_t peer_keys_len
);

/** @brief Find the EDHOC context associated to an incoming message
 *
 * If using a correlation value different from 0, this method is not guaranteed
 * to find the matching context because the connection identifier may not be
 * part of the message. Instead, the external correlation mechanism will have
 * to match requests and replies e.g. with a CoAP token.
 *
 * If the message is an EDHOC message 1, a new context will have to be created.
 *
 * @param[in] ctx_list Context list to search through
 * @param[in] ctx_list_len Length of @p ctx_list
 * @param[in] buffer The message to analyze
 * @param[in] buffer_len Length of @p buffer
 * @param[out] ctx The found EDHOC context (if any)
 */
edhoc_error_t edhoc_find_context(
        edhoc_context_t **ctx_list,
        size_t ctx_list_len,
        const uint8_t *buffer,
        size_t buffer_len,
        edhoc_context_t **ctx
);

/** @brief Produce the initial EDHOC message
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[out] buffer Output buffer where the message is written to
 * @param[in] buffer_len Length of @p buffer
 * @param[out] msg_len Length of the final message
 */
edhoc_error_t edhoc_start_protocol(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
);

/** @brief Treat an EDHOC message and react in an appropriate way
 *
 * The method detects the current protocol state and produces either the next
 * EDHOC message, an EDHOC error message or if a local error occurs, nothing is
 * produced but an error value is returned.
 *
 * If an EDHOC error message was produced (whether this is the case can be
 * checked with @ref edhoc_error_message_produced;), it should be sent to the
 * peer to inform it of the failure and to restart the protocol.
 *
 * @note The input buffer is modified if necessary (to decrypt the payload)
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[inout] buf_in Input buffer containing the message to react to
 * @param[in] buf_in_len Length of @p buf_in
 * @param[out] buf_out Output buffer where the response is written to
 * @param[in] buf_out_len Length of @p buf_out
 * @param[out] msg_out_len Length of the final response message
 */
edhoc_error_t edhoc_handle_message(
        edhoc_context_t *ctx,
        uint8_t *buf_in,
        size_t buf_in_len,
        uint8_t *buf_out,
        size_t buf_out_len,
        size_t *msg_out_len
);

/** @brief Indicates if an error message was produced as a response
 *
 * Call this method with the return value of edhoc_handle_[coap_]message.
 * Even if the incoming message could not be handled correctly, an error message
 * might have been produced, which should be sent to the peer to announce the
 * failure and to restart the protocol.
 *
 * @param[in] error The error value received by edhoc_handle_[coap_]message
 * @return true if an error message was produced, false otherwise.
 */
bool edhoc_error_message_produced(edhoc_error_t error);

/** @brief Get the actual error description (text) out of an EDHOC error message
 *
 * @param[in] buffer The error message
 * @param[in] buffer_len Length of @p buffer
 * @param[out] reason Output, where the description pointer will be placed in
 * @param[out] reason_len Length of the description text
 */
edhoc_error_t edhoc_get_err_msg_reason(
        const uint8_t *buffer,
        size_t buffer_len,
        const char **reason,
        size_t *reason_len
);

/** @brief Check whether the protocol has been completed and the OSCORE context parameters can be derived
 *
 * Even if the protocol is assumed to be finished, party V might send back an
 * error message upon receiving message 3, in which case the protocol failed.
 *
 * @param[in] ctx EDHOC context to use
 * @return true if finished, false otherwise
 */
bool edhoc_protocol_finished(edhoc_context_t *ctx);

/** @brief Derive application parameters after successful completion of EDHOC
 *
 * This method only succeeds after the EDHOC protocol is completed, meaning
 * that EDHOC message 3 needs to be sent / received.
 *
 * @param[in] ctx EDHOC context to use
 * @param[in] label Parameter label, e.g. "Application secret"
 * @param[in] label_len Length of @p label
 * @param[out] output Buffer where the parameter is written to
 * @param[in] output_len Length of the parameter to export
 */
edhoc_error_t edhoc_exporter(
        edhoc_context_t *ctx,
        uint8_t *label,
        size_t label_len,
        uint8_t *output,
        size_t output_len
);

/** @brief Derive parameters used to create an OSCORE context after successful completion of EDHOC
 *
 * The @p master_ctx length fields need to be set correctly.
 * This method only succeeds after the EDHOC protocol is completed, meaning
 * that EDHOC message 3 needs to be sent / received.
 *
 * @param[in] ctx EDHOC context to use
 * @param[inout] master_ctx Structure holding length information & buffers where the parameters are written to
 */
edhoc_error_t edhoc_derive_oscore_params(
        edhoc_context_t *ctx,
        struct oscore_master_context *master_ctx
);

/** @} */

#endif //EDHOC_IMPLEMENTATION_EDHOC_H
