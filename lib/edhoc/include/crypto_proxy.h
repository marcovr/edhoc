#ifndef EDHOC_CRYPTO_PROXY_H
#define EDHOC_CRYPTO_PROXY_H

/** @file */

/** @ingroup oscore_extensions
 *  @addtogroup edhoc_crypto_proxy Cryptography API proxy
 *
 *  @brief Cryptography API proxy, which extends the OSCORE native Cryptography API
 *
 *  See: https://marcovr.gitlab.io/oscore-implementation/group__oscore__native__crypto.html
 *
 *  This proxy is partially used to add functionality which at the moment isn't
 *  available in OSCORE. Once this changes, the proxy be bypassed for those
 *  functions. Also, some EDHOC-specific crypto functions are provided.
 *
 *  @note Some functionality is provided by libcose, thus the proxy only works
 *  in conjunction with this library. Per default, it is included in the oscore
 *  library.
 *
 * @{
 */

#include <stdint.h>

#include <oscore/helpers.h>
#include <oscore_native/crypto.h>
#include <crypto_proxy_type.h>

#define edhoc_crypto_aead_from_number oscore_crypto_aead_from_number
#define edhoc_crypto_aead_get_taglength oscore_crypto_aead_get_taglength
#define edhoc_crypto_aead_get_ivlength oscore_crypto_aead_get_ivlength
#define edhoc_cryptoerr_is_error oscore_cryptoerr_is_error
#define edhoc_crypto_aead_encrypt_start oscore_crypto_aead_encrypt_start
#define edhoc_crypto_aead_encrypt_feed_aad oscore_crypto_aead_encrypt_feed_aad
#define edhoc_crypto_aead_encrypt_inplace oscore_crypto_aead_encrypt_inplace
#define edhoc_crypto_aead_decrypt_start oscore_crypto_aead_decrypt_start
#define edhoc_crypto_aead_decrypt_feed_aad oscore_crypto_aead_decrypt_feed_aad
#define edhoc_crypto_aead_decrypt_inplace oscore_crypto_aead_decrypt_inplace
#define edhoc_crypto_hkdf_derive oscore_crypto_hkdf_derive

/** @brief Set up an algorithm descriptor from a numerically identified COSE
 * Direct Key with KDF
 *
 * @param[out] alg Output field for the algorithm
 * @param[in] number Algorithm number from IANA's COSE Algorithms registry
 *
 * If the number given is not an HKDF algorithm or not implemented, the
 * function must return an error; it may leave @p alg uninitialized or set it
 * arbitrarily in that case.
 *
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_hkdf_from_number(edhoc_crypto_hkdfalg_t *alg, int32_t number);

/** @brief Start a hash operation
 *
 * @param[out] state Hash state to set up
 * @param[in] alg OSCORE hash algorithm that will be used
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_hash_init(
        edhoc_crypto_hashstate_t *state,
        edhoc_crypto_hashalg_t alg
);

/** @brief Provide a data chunk to an ongoing hash operation
 *
 * @param[inout] state Hash state to update
 * @param[in] input Data chunk to be processed
 * @param[in] length Length of @p input
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_hash_update(
        edhoc_crypto_hashstate_t *state,
        const uint8_t *input,
        size_t length
);

/** @brief Finish a hash operation
 *
 * The output buffer needs to be large enough for the hash value to fit inside.
 *
 * @param[inout] state Hash state to use and finalize
 * @param[out] hash Buffer where the final hash value is written to
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_hash_final(
        edhoc_crypto_hashstate_t *state,
        uint8_t *hash
);

/** @brief Sign (authenticate) a message with a signature created using an elliptic curve private key
 *
 * The output buffer needs to be large enough for the signature to fit inside.
 *
 * @param[in] alg Curve algorithm to use
 * @param[in] input Message to sign
 * @param[in] input_len Length of @p input
 * @param[in] privkey Private key used to create the signature
 * @param[in] pubkey Corresponding public key used to create the signature
 * @param[out] signature Buffer where the final signature is written to
 * @param[out] signature_len Length of the final signature
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_sign(
        edhoc_crypto_curvealg_t alg,
        uint8_t *input,
        size_t input_len,
        const uint8_t *privkey,
        const uint8_t *pubkey,
        uint8_t *signature,
        size_t *signature_len
);

/** @brief Verify the signature of a message. Analogous to @ref oscore_crypto_sign; but instead of creating the
 * signature, it is checked whether the given signature matches.
 *
 * @param[in] alg Curve algorithm to use
 * @param[in] input Message to check signature against
 * @param[in] input_len Length of @p input
 * @param[in] pubkey Public key which was used to create the signature
 * @param[in] signature The signature to verify
 * @param[in] signature_len Length of @p signature
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_sign_verify(
        edhoc_crypto_curvealg_t alg,
        uint8_t *input,
        size_t input_len,
        const uint8_t *pubkey,
        const uint8_t *signature,
        size_t signature_len
);

/** @brief Derive a shared secret from a foreign public key and your own private key
 *
 * The output buffer needs to be large enough for the secret to fit inside.
 *
 * @param[in] alg Curve algorithm to use
 * @param[out] secret Buffer where the shared secret is written to
 * @param[in] privkey Your private key used to derive secret
 * @param[in] pubkey Foreign public key used to derive secret
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_shared_secret(
        edhoc_crypto_curvealg_t alg,
        uint8_t *secret,
        const uint8_t *privkey,
        const uint8_t *pubkey
);

/** @brief Make a new (ephemeral) keypair
 *
 * The output buffers need to be large enough for the keys to fit inside.
 *
 * @param[in] alg Curve algorithm to use
 * @param[out] coord_x Buffer where the public key is written to
 * @param[out] private Buffer where the private key is written to
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_make_keypair(
        edhoc_crypto_curvealg_t alg,
        uint8_t *coord_x,
        uint8_t *private
);

/** @brief Get hash type from COSE HMAC number
 *
 * @param[out] alg The hash algorithm
 * @param[in] number The COSE HMAC number
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_hash_from_hmac_number(
        edhoc_crypto_hashalg_t *alg,
        int32_t number
);

/** @brief Get hash type from COSE curve algorithm number
 *
 * @param[out] alg The curve algorithm
 * @param[in] number The COSE curve algorithm number
 */
OSCORE_NONNULL
edhoc_cryptoerr_t edhoc_crypto_curvealg_from_number(
        edhoc_crypto_curvealg_t *alg,
        int32_t number
);

/** @brief Get the maximum size for the given signature algorithm
 *
 * @param[in] alg The signature algorithm
 */
OSCORE_NONNULL
size_t edhoc_crypto_curvealg_get_signlength(edhoc_crypto_curvealg_t alg);

/** @brief Verify whether the coordinate is valid or not
 *
 * @param[in] alg The curve algorithm
 * @param[in] coord_x Coordinate to validate
 * @return True if valid, false otherwise
 */
OSCORE_NONNULL
bool edhoc_crypto_validate_coord(edhoc_crypto_curvealg_t alg, const uint8_t *coord_x);

/** @} */

#endif //EDHOC_CRYPTO_PROXY_H
