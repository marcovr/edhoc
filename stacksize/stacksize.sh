#!/bin/bash
# Collects all build files *.o *.su *.dfinish and runs the WCS script to
# estimate the stack usage

EDH=../build/CMakeFiles/edhoc-demo.dir
OSC=../lib/oscore/oscore-implementation/tests/native

mkdir -p buildfiles

find "$EDH" -type f \( -name "*.o" -or -name "*.su" -or -name "*.dfinish" \) -print0 | xargs -0 cp --target-directory=buildfiles
(
  cd buildfiles || exit
  for file in *.c.o; do mv "$file" "${file/.c/}"; done
  for file in *.c.su; do mv "$file" "${file/.c/}"; done
)
cp $OSC/*.o $OSC/*.su $OSC/*.dfinish buildfiles

# Libsodium uses recursive functions which are not supported by the WCS script
#find ../../libsodium/src/ -path "*/.libs" -prune -o -type f \( -name "*.o" -or -name "*.su" -or -name "*.dfinish" \) -print0 | xargs -0 cp --target-directory=.

#for file in libsodium_la-*; do mv "$file" "${file/libsodium_la-/}"; done
#for file in libaesni_la-*; do mv "$file" "${file/libaesni_la-/}"; done
#for file in libavx2_la-*; do mv "$file" "${file/libavx2_la-/}"; done
#for file in libavx512f_la-*; do mv "$file" "${file/libavx512f_la-/}"; done
#for file in librdrand_la-*; do mv "$file" "${file/librdrand_la-/}"; done
#for file in libsse2_la-*; do mv "$file" "${file/libsse2_la-/}"; done
#for file in libsse41_la-*; do mv "$file" "${file/libsse41_la-/}"; done
#for file in libssse3_la-*; do mv "$file" "${file/libssse3_la-/}"; done

python3 WCS.py > stacksize.txt
sed -n -r -e 's/^.+ +main +(unbounded:)?([0-9]+) +.+$/\2/p' stacksize.txt

rm -r buildfiles
