#include <examples_shared.h>
#include <stdarg.h>
#include <debug.h>

#ifndef ESP_PLATFORM
void log_info(const char *tag, const char *fmt, ...) {
    printf("%s: ", tag);
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf("\n");
}

void log_err(const char *tag, const char *fmt, ...) {
    fprintf(stderr, "%s: ", tag);
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    fprintf(stderr, "\n");
}
#endif

void edhoc_success(const char *tag, edhoc_context_t *ctx) {
    log_info(tag, "EDHOC protocol succeeded!");

    struct oscore_master_context master_ctx = {
            .master_secret_len = 16,
            .master_salt_len = 8
    };
    edhoc_error_t res = edhoc_derive_oscore_params(ctx, &master_ctx);
    if (res == EDHOC_OK) {
        dump("OSCORE parameters (key, salt)", master_ctx.master_secret, master_ctx.master_secret_len);
        dump(NULL, master_ctx.master_salt, master_ctx.master_salt_len);
    } else {
        log_err(tag, "But failed to derive OSCORE parameters");
    }
}

void edhoc_failed(
        const char *tag,
        edhoc_error_t err,
        coap_pdu_t *msg_in,
        coap_pdu_t *msg_out
) {
    const char *err_desc;
    size_t err_desc_len = 0;
    log_err(tag, "EDHOC protocol failed");
    if (err == EDHOC_ERROR_IS_ERROR_MSG) {
        edhoc_get_coap_err_msg_reason(msg_in, &err_desc, &err_desc_len);
        log_err(tag, "Received error message: %.*s", err_desc_len, err_desc);
    } else {
        edhoc_get_coap_err_msg_reason(msg_out, &err_desc, &err_desc_len);
        log_err(tag, "Error (code %d): %.*s", err, err_desc_len, err_desc);
    }
}

bool init_oscore(edhoc_context_t *ctx, oscore_context_t *secctx) {
    struct oscore_master_context master_ctx = {
            .master_secret_len = 16,
            .master_salt_len = 8
    };
    edhoc_error_t res = edhoc_derive_oscore_params(ctx, &master_ctx);
    return res == EDHOC_OK && oscore_context_derive_keys(secctx, &master_ctx);
}
