
# EDHOC/OSCORE server example

This EDHOC/OSCORE server example is based one the
[CoAP server example](https://github.com/espressif/esp-idf/tree/master/examples/protocols/coap_server)
of [ESP-IDF](https://github.com/espressif/esp-idf) and intended to run on an
ESP32 device.

The EDHOC/OSCORE server example will startup a daemon task, receive requests over CoAP
(UDP on port 5684) from EDHOC/OSCORE clients and transmit responses.

## How to use example

### Configure the project

```
idf.py menuconfig
```

Example Connection Configuration  --->
 * Set WiFi SSID under Example Configuration
 * Set WiFi Password under Example Configuration

Example EDHOC Client Configuration  --->
 * Set the EDHOC endpoint if you want to change it

Component config  ---> CoAP Configuration  --->
 * Enable CoAP debugging if required

### Build and Flash using ESP-IDF

The first time, the OSCORE dependencies need to be cloned, to do that, run `../setup.sh`

Build the project and flash it to the board, then run monitor tool to view serial output:

```
idf.py build
idf.py -p PORT flash monitor
# PORT is usually /dev/ttyUSB0
```

(To exit the serial monitor, type ``Ctrl-]``.)

See the Getting Started Guide for full steps to configure and use ESP-IDF to build projects.

### Alternatively, Build and Flash using PlatformIO
The first time, the OSCORE dependencies need to be cloned, to do that, run `../setup.sh`.
Additionally, after having configured the project (see above), run `../transform.sh` to
generate the necessary sdkconfig header.

Build the project and flash it to the board, then run monitor tool to view serial output:

```
platformio run -t upload -t monitor -e esp32thing
```

## Example Output
A successful run (if a client connects) could look like that:
```
...
I (2082) wifi: pm start, type: 1

I (3172) tcpip_adapter: sta ip: 192.168.0.122, mask: 255.255.255.0, gw: 192.168.0.1
I (3672) example_connect: Connected to AP
I (3672) example_connect: IPv4 address: 192.168.0.120
I (3682) EDHOC/OSCORE_server: server is ready
I (20892) EDHOC/OSCORE_server: EDHOC protocol succeeded!
OSCORE parameters (key, salt):
0x8d67714881beb5af87acd0417c5990f8
0x1cbcdf49af989e86
> ping!
< pong!
I (21032) EDHOC/OSCORE_server: OSCORE communication succeeded!
...
```
