// Adapted from the ESP-IDF coap client example
// https://github.com/espressif/esp-idf/blob/master/examples/protocols/coap_client/main/coap_client_example_main.c
#include <examples_shared.h>
#ifdef ESP_PLATFORM
#include <sys/socket.h>
#include <netdb.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
#else
#include <netdb.h>
#include <arpa/inet.h>
#endif

#ifdef ESP_PLATFORM
#define COAP_SERVER_URI CONFIG_EXAMPLE_TARGET_DOMAIN_URI
#define COAP_LOG_LEVEL CONFIG_COAP_LOG_DEFAULT_LEVEL
#else
#define COAP_SERVER_URI "coap://localhost/.well-known/edhoc"
#define COAP_LOG_LEVEL LOG_WARNING
#endif
#define COAP_TIMEOUT_SEC 5


static const uint8_t AUTH_U_PRIVATE[32] = {
        0x53, 0x21, 0xFC, 0x01, 0xC2, 0x98, 0x20, 0x06, 0x3A, 0x72, 0x50,
        0x8F, 0xC6, 0x39, 0x25, 0x1D, 0xC8, 0x30, 0xE2, 0xF7, 0x68, 0x3E,
        0xB8, 0xE3, 0x8A, 0xF1, 0x64, 0xA5, 0xB9, 0xAF, 0x9B, 0xE3
};
static const uint8_t AUTH_U_PUBLIC[32] = {
        0x42, 0x4C, 0x75, 0x6A, 0xB7, 0x7C, 0xC6, 0xFD, 0xEC, 0xF0, 0xB3,
        0xEC, 0xFC, 0xFF, 0xB7, 0x53, 0x10, 0xC0, 0x15, 0xBF, 0x5C, 0xBA,
        0x2E, 0xC0, 0xA2, 0x36, 0xE6, 0x65, 0x0C, 0x8A, 0xB9, 0xC7
};
static const uint8_t AUTH_V_PUBLIC[32] = {
        0x1B, 0x66, 0x1E, 0xE5, 0xD5, 0xEF, 0x16, 0x72, 0xA2, 0xD8, 0x77,
        0xCD, 0x5B, 0xC2, 0x0F, 0x46, 0x30, 0xDC, 0x78, 0xA1, 0x14, 0xDE,
        0x65, 0x9C, 0x7E, 0x50, 0x4D, 0x0F, 0x52, 0x9A, 0x6B, 0xD3
};
static const uint8_t KID_U[1] = {0xA2};
static const uint8_t KID_V[1] = {0xA3};

static edhoc_auth_keypair_t keypair = {
        .kid = KID_U,
        .public = AUTH_U_PUBLIC,
        .private = AUTH_U_PRIVATE,
        .kid_len = sizeof(KID_U)
};
static edhoc_auth_keypair_t peer_key = {
        .kid = KID_V,
        .public = AUTH_V_PUBLIC,
        .kid_len = sizeof(KID_V)
};
static edhoc_auth_keypair_t *peer_keys[1] = {&peer_key};
static edhoc_context_t edhoc_ctx;


const static char *TAG = "EDHOC_client";

static bool resp_wait = true;
static coap_optlist_t *optlist = NULL;
static int wait_ms;


static void create_edhoc_request(coap_session_t *session, coap_pdu_t *received) {
    coap_pdu_t *request = coap_new_pdu(session);
    if (!request) {
        log_err(TAG, "coap_new_pdu() failed");
        resp_wait = false;
        return;
    }
    request->type = COAP_MESSAGE_CON;
    request->tid = coap_new_message_id(session);
    request->code = COAP_REQUEST_POST;
    coap_add_optlist_pdu(request, &optlist);

    edhoc_context_t *ctx = &edhoc_ctx;
    edhoc_error_t res;
    if (received == NULL) {
        res = edhoc_init_context(ctx, &keypair, peer_keys, 1);
        assert(res == EDHOC_OK);
        res = edhoc_start_protocol_coap(ctx, request);
    } else {
        res = edhoc_handle_coap_message(ctx, received, request);
    }

    if (res != EDHOC_OK) {
        edhoc_failed(TAG, res, received, request);
    } else if (edhoc_protocol_finished(ctx)) {
        log_info(TAG, "Waiting for confirmation");
    }

    size_t data_len;
    uint8_t *data;
    if (coap_get_data(request, &data_len, &data)) {
        coap_send(session, request);
        wait_ms = COAP_TIMEOUT_SEC * 1000;
    } else {
        coap_delete_pdu(request);
        resp_wait = false;
    }
}

static void message_handler(coap_context_t *cctx, coap_session_t *session,
                            coap_pdu_t *sent, coap_pdu_t *received,
                            const coap_tid_t id) {
    (void)cctx;
    (void)sent;
    (void)id;

    uint8_t *data;
    size_t data_len;

    if (received->code != COAP_CODE_CHANGED) {
        return;
    }
    if (!coap_get_data(received, &data_len, &data)) {
        if (edhoc_protocol_finished(&edhoc_ctx)) {
            edhoc_success(TAG, &edhoc_ctx);
            resp_wait = false;
        }
        return;
    }

    create_edhoc_request(session, received);
}

static void coap_example_client(void *p) {
    (void) p;

    struct hostent *hp;
    coap_address_t dst_addr;
    static coap_uri_t uri;
    const char *server_uri = COAP_SERVER_URI;
    char *phostname = NULL;

    coap_set_log_level(COAP_LOG_LEVEL);

#define BUFSIZE 40
    unsigned char _buf[BUFSIZE];
    unsigned char *buf;
    size_t buflen;
    int res;
    coap_context_t *ctx = NULL;
    coap_session_t *session = NULL;

    optlist = NULL;
    if (coap_split_uri((const uint8_t *)server_uri, strlen(server_uri), &uri) == -1) {
        log_err(TAG, "CoAP server uri error");
        return;
    }

    if (uri.scheme != COAP_URI_SCHEME_COAP) {
        log_err(TAG, "Provided CoAP scheme not supported, use coap://...");
        return;
    }

    phostname = (char *)calloc(1, uri.host.length + 1);
    if (phostname == NULL) {
        log_err(TAG, "calloc failed");
        return;
    }

    memcpy(phostname, uri.host.s, uri.host.length);
    hp = gethostbyname(phostname);
    free(phostname);

    if (hp == NULL) {
        log_err(TAG, "DNS lookup failed");
        return;
    }
    char tmpbuf[INET6_ADDRSTRLEN];
    coap_address_init(&dst_addr);
    switch (hp->h_addrtype) {
        case AF_INET:
            dst_addr.addr.sin.sin_family = AF_INET;
            dst_addr.addr.sin.sin_port = htons(uri.port);
            memcpy(&dst_addr.addr.sin.sin_addr, hp->h_addr, sizeof(dst_addr.addr.sin.sin_addr));
            inet_ntop(AF_INET, &dst_addr.addr.sin.sin_addr, tmpbuf, sizeof(tmpbuf));
            log_info(TAG, "DNS lookup succeeded. IP=%s", tmpbuf);
            break;
        case AF_INET6:
            dst_addr.addr.sin6.sin6_family = AF_INET6;
            dst_addr.addr.sin6.sin6_port = htons(uri.port);
            memcpy(&dst_addr.addr.sin6.sin6_addr, hp->h_addr, sizeof(dst_addr.addr.sin6.sin6_addr));
            inet_ntop(AF_INET6, &dst_addr.addr.sin6.sin6_addr, tmpbuf, sizeof(tmpbuf));
            log_info(TAG, "DNS lookup succeeded. IP=%s", tmpbuf);
            break;
        default:
            log_err(TAG, "DNS lookup response failed");
            goto clean_up;
    }

    if (uri.path.length) {
        buflen = BUFSIZE;
        buf = _buf;
        res = coap_split_path(uri.path.s, uri.path.length, buf, &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_PATH,
                                                 coap_opt_length(buf),
                                                 coap_opt_value(buf)));

            buf += coap_opt_size(buf);
        }
    }

    if (uri.query.length) {
        buflen = BUFSIZE;
        buf = _buf;
        res = coap_split_query(uri.query.s, uri.query.length, buf, &buflen);

        while (res--) {
            coap_insert_optlist(&optlist,
                                coap_new_optlist(COAP_OPTION_URI_QUERY,
                                                 coap_opt_length(buf),
                                                 coap_opt_value(buf)));

            buf += coap_opt_size(buf);
        }
    }

    ctx = coap_new_context(NULL);
    if (!ctx) {
        log_err(TAG, "coap_new_context() failed");
        goto clean_up;
    }

    session = coap_new_client_session(ctx, NULL, &dst_addr, COAP_PROTO_UDP);
    if (!session) {
        log_err(TAG, "coap_new_client_session() failed");
        goto clean_up;
    }

    coap_register_response_handler(ctx, message_handler);
    create_edhoc_request(session, NULL);

    while (resp_wait) {
        int result = coap_run_once(ctx, wait_ms > 1000 ? 1000 : wait_ms);
        if (result >= 0) {
            if (result >= wait_ms) {
                log_err(TAG, "select timeout");
                break;
            } else {
                wait_ms -= result;
            }
        }
    }
    clean_up:
    if (optlist) {
        coap_delete_optlist(optlist);
        optlist = NULL;
    }
    if (session) {
        coap_session_release(session);
    }
    if (ctx) {
        coap_free_context(ctx);
    }
    coap_cleanup();
#ifdef ESP_PLATFORM
    vTaskDelete(NULL);
#endif
}

#ifdef ESP_PLATFORM
void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    xTaskCreate(coap_example_client, "coap", 8 * 1024, NULL, 5, NULL);
}
#else
int main() {
    coap_example_client(NULL);
    return 0;
}
#endif
