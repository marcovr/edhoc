set(OSCORE_S ${CMAKE_CURRENT_SOURCE_DIR}/../../../lib/oscore/oscore-implementation)
set(OSCORE_LIBS ${OSCORE_S}/tests/native/libs)

idf_component_register(SRCS ${OSCORE_S}/src/contextpair.c ${OSCORE_S}/src/protection.c ${OSCORE_S}/src/oscore_message.c
        ${OSCORE_S}/src/cbor.c ${OSCORE_S}/backends/libcoap/src/oscore_msg.c ${OSCORE_S}/backends/libcose/src/libcose.c
        ${OSCORE_LIBS}/libcose/src/cose_crypto.c ${OSCORE_LIBS}/libcose/src/crypt/sodium.c ${OSCORE_LIBS}/libcose/src/crypt/tc_aes.c
        INCLUDE_DIRS ${OSCORE_LIBS}/libcose/include ${OSCORE_LIBS}/nanocbor/include ${OSCORE_S}/backends/libcose/inc
        ${OSCORE_S}/backends/libcoap/inc ${OSCORE_S}/src/include
        REQUIRES mbedtls libsodium coap)

target_compile_options(${COMPONENT_LIB} PRIVATE -DCRYPTO_SODIUM=1 -DCRYPTO_TC_AES)
