#!/bin/bash
# Produces a header file (sdkconfig.h) out of an SDK config file
# Even though the TLS-server is not used, it needs to marked as active since otherwise PlatformIO fails to compile the examples

{
  sed -r -e 's/#.*$//' -e '/^$/d' -e 's/^(\w+)=y$/\1=1/' -e 's/^(\w+)=/\1 /' -e 's/^/#define /' sdkconfig
  echo "#define CONFIG_LOG_TIMESTAMP_SOURCE_SYSTEM 1"
  echo "#define CONFIG_ESP_TLS_SERVER 1"
} > sdkconfig.h
