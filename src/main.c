#include <stdio.h>
#ifdef ESP_PLATFORM
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_event.h"
#include "nvs_flash.h"
#endif

extern int testmain(int introduce_error);


#ifdef ESP_PLATFORM
void esp_run() {
    int res = testmain(0);
    if (res == 0) {
        printf("Success\n");
    } else {
        fprintf(stderr, "Failed!\n");
    }
}

void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    xTaskCreate(esp_run, "edhoc", 8 * 1024, NULL, 5, NULL);
}
#else
int main(int argc, char *argv[]) {
    int res = testmain(0);
    if (res == 0) {
        printf("Success\n");
    } else {
        fprintf(stderr, "Failed!\n");
    }
    return res;
}
#endif
